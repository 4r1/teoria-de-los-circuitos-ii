#!/bin/python2
# -*- coding: utf-8 -*-

import argparse
import os
import subprocess

content = r'''
%% Notese que el documento esta calibrado para tamaño de hoja ISO A4;
%% Si se desea usar otro tamaño de hoja, debera modificar los valores
%% xscale, yscale dentro de \begin{scope}
\documentclass[a4paper, 11pt]{report}
\usepackage{rotating}
\usepackage[margin=0mm]{geometry}
\usepackage{tikz}
\usetikzlibrary{fit,intersections,shapes,calc}
\definecolor{grisFeo}{gray}{0.5}
\definecolor{grisFeaso}{gray}{0.2}


%% Valores a Cambiar
%% Cuantas decadas logaritmicas debe tener el grafico
\def\decadas{%(horiz)s}
%% Cuantas divisiones de dB debe tener el grafico
\def\escalaVertical{%(verti)s}
%% Cuantas subdivisiones de dB debe tener el grafico
\def\resolucionVertical{%(resol)s}


\pgfmathparse{(\escalaVertical * 20)}\let\dBpp\pgfmathresult
\pgfmathparse{(20 / \resolucionVertical)}\let\suavizado\pgfmathresult
\pgfmathparse{(\suavizado / 5)}\let\suavizadoFino\pgfmathresult
\pgfmathparse{(10 / \decadas * 2.7)}\let\escaladoHorizontal\pgfmathresult
\pgfmathparse{(8 / \escalaVertical * 0.12)}\let\escaladoVertical\pgfmathresult

\newcommand{\semilog}[3][]{
  \pgfmathparse{int(#2-1)}\let\decadaMax\pgfmathresult

  \foreach \dBActual in  {0,\suavizadoFino,...,#3}{
    \draw[line width=0.0mm,  lightgray!80] (0,\dBActual) -- ({#2},\dBActual);};
  \foreach \dBActual in  {0,\suavizado,...,#3}{
    \draw[line width=0.10mm, black] (0,\dBActual) -- ({#2},\dBActual);};

  \foreach \decadaActual in{0,...,\decadaMax}{
    \foreach \x in {1,2,3,4}{
    \foreach \y in {5}{
      \draw[line width=0.0mm, gray!80] ({log10(\x + \y / 10)+\decadaActual},0) -- ({log10(\x + \y / 10)+\decadaActual},#3);}
    }
    \foreach \x in {5,6,7,8,9}{
    \foreach \y in {5}{
      \draw[line width=0.0mm, gray!80] ({log10(\x + \y / 10)+\decadaActual},0) -- ({log10(\x + \y / 10)+\decadaActual},#3);}
    }
    \foreach \x in {2,3,4,5}{
      \ifnum11>\decadas
      \draw[line width=0.20mm, black] ({log10(\x)+\decadaActual},0)node[below, black]{\x} -- ({log10(\x)+\decadaActual},#3);
    \else
      \draw[line width=0.20mm, black] ({log10(\x)+\decadaActual},0)node[below, black]{} -- ({log10(\x)+\decadaActual},#3);
    \fi}
    \foreach \x in {6,7,8,9}{
      \draw[line width=0.20mm, black] ({log10(\x)+\decadaActual},0)node[below, black]{} -- ({log10(\x)+\decadaActual},#3);}
    \draw[line width=0.3mm, black] ({1+ \decadaActual},0)node[below, black]{{\textbf{10}}$^{[\;\;\;]}$} -- ({1+ \decadaActual},#3);
  };
  \draw[line width=0.3mm, black] (0,0)node[black, below]{{\textbf{10}}$^{[\;\;\;]}$} -- (0,#3);
  \foreach \dBActual in  {0,20,...,#3}{
    \draw[line width=0.4mm, black] (0,\dBActual) -- ({#2},\dBActual);};
}

\begin{document}
\thispagestyle{empty}

%% descomentar abajo para tambien dejar un pequeño espacio a la derecha
%% \hspace{1pt} \vfill
\begin{center} \begin{turn}{90}

\begin{tikzpicture}
\begin{scope}[xscale=\escaladoHorizontal,yscale=\escaladoVertical]
  \semilog{\decadas}{\dBpp}
\end{scope}
\end{tikzpicture}

\end{turn} \end{center} \vfill \hspace{1pt}

\end{document}
'''

parser = argparse.ArgumentParser(description='Generador de hojas semilogaritmicas')
parser.add_argument('-x', '--horiz', default=9, type=int, choices=xrange(1,1024), help='Decadas logaritmicas en el eje horizontal')
parser.add_argument('-y', '--verti', default=6, type=int, choices=xrange(1,1024), help='Divisiones en el eje vertical')
parser.add_argument('-r', '--resol', default=9, type=int, choices=xrange(0,1024), help='Subdivisiones en dB en el eje horizontal')

args = parser.parse_args()

with open('hojaLog.tex','w') as f:
    f.write(content%args.__dict__)

cmd = ['pdflatex', '-interaction', 'nonstopmode', 'hojaLog.tex']
proc = subprocess.Popen(cmd)
proc.communicate()
retcode = proc.returncode
if not retcode == 0:
    os.unlink('hojaLog.pdf')
    raise ValueError('Error {} executing command: {}'.format(retcode, ' '.join(cmd)))

os.unlink('hojaLog.tex')
os.unlink('hojaLog.log')
os.unlink('hojaLog.aux')
