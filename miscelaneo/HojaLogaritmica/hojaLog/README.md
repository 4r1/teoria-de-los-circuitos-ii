# Generador de hoja semilogarítmica
# Instalacion de paquetes necesarios
## Dependencia de TexLive
Antes de ejecutar el script será necesario tener instalado el paquete *TexLive*. Puede verificar la existencia del mismo ejecutando:
```
pdflatex --version
```
Si le aparece algo al estilo de:
```
bash: pdflatex: command not found
```
significa que no tiene TexLive instalado, proceda a instalarlo con el siguiente comando para Ubuntu o Debian:

```
sudo apt install texlive
```
O con el siguiente comando si usted tiene Fedora:
```
sudo dnf install texlive
```
En caso de tener alguna otra distribucion, use el manejador de paquetes adecuado.

## Dependencia de Python2
Tambien debe tener instalado Python2, lo cual puede verificar con el siguiente comando:
```
python2 --version
```
Si le aparece algo al estilo de:
```
bash: python2: command not found
```
significa que no tiene TexLive instalado, proceda a instalarlo con el siguiente comando para Ubuntu o Debian:

```
sudo apt install python2.7
```
O con el siguiente comando si usted tiene Fedora:
```
sudo dnf install python2
```
En caso de tener alguna otra distribucion, use el manejador de paquetes adecuado.

# Instrucciones de Uso
Para ejecutar este script será necesario convertir el archivo *hojaLog.py* en ejecutable mediante el bash localizado en el directorio contenedor, ejecutando el siguiente comando:
```
chmod +x hojaLog.py
```
Finalmente se procederá a ejecutar el archivo transfiriendole los siguientes parámetros:
* **Número de decadas en escala logarítmica en el eje horizontal:** [-x] o [--horiz]
* **Número de divisiones en el eje vertical:** [-y] o [--verti]
* **Resolución del eje vertical (número de subdivisiones dentro de cada división del eje vertical - Escala en decibeles):** [-r] o [--resol]
Los parámetros son números enteros.

## Ejemplo

El siguiente ejemplo usa los valores por defecto, y guarda en el directorio contenedor del script *hojaLog.py* el archivo *hojaLog.pdf* con 8 decadas graduadas en escala logarítmica, con 2 divisiones por década y 10 divisiones en el eje lineal vertical.
```
./hojaLog.py
```

El siguiente ejemplo guarda en el directorio contenedor del script *hojaLog.py* el archivo *hojaLog.pdf* con 5 decadas graduadas en escala logarítmica, con 6 divisiones por década y 7 divisiones en el eje lineal vertical.
```
./hojaLog.py -x 5 -y 6 -r 7
```
